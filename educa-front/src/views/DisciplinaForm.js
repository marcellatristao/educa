import React, { Component } from 'react';
import { Formik, ErrorMessage, Form, Field } from 'formik';
import { Link } from 'react-router-dom';

import { connect } from "react-redux";
import { salvarDisciplina } from "../store/actions/DisciplinaActions";
import { disciplinaSelector } from '../store/selectors/DisciplinaSelectors';

const initialValues = {
    nome: "",
    codigo: "",
    carga_horaria: ""
}

class DisciplinaForm extends Component {

    componentDidMount(){
        const {id} = this.props.match.params;
    }

    handleSubmit(values, {setSubmitting}, props){

        this.props.salvarDisciplina(values);

        //habilitando o btn salvar
        setSubmitting(false);

        //redireciona para a página anterior
        props.history.goBack();
    }

    render() {
        return (
            <div>

                <Formik
                    initialValues = { this.props.disciplina || initialValues }
                    onSubmit= {(values, actions) => this.handleSubmit(values, actions, this.props)}
                >
                    { ({values, handleSubmit, isSubmitting}) => ( 
                        
                        <Form onSubmit={handleSubmit}>

                            <div>
                                <label>Nome: </label>
                                <Field type="text" name="nome"/>
                                <ErrorMessage name="nome" component="div"/>
                            </div>

                            <div>
                                <label>Código: </label>
                                <Field type="text" name="codigo"/>
                                <ErrorMessage name="codigo" component="div"/>
                            </div>

                            <div>
                                <label>Carga Horária: </label>
                                <Field type="text" name="carga_horaria"/>
                                <ErrorMessage name="carga_horaria" component="div"/>
                            </div>

                            <div>
                                <button type="submit" className="btn btn-primary" disabled={isSubmitting}>Salvar</button>
                                <Link to="/disciplina/list" className="btn btn-danger">Cancelar</Link>
                            </div>

                        </Form>

                    )}



                </Formik>
            </div>
        );
    }

}

export default connect (disciplinaSelector, {salvarDisciplina})(DisciplinaForm);