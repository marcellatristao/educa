import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import { connect } from "react-redux";
import { listarDisciplina } from "../store/actions/DisciplinaActions";
import { disciplinaListSelector } from "../store/selectors/DisciplinaSelectors";

const id = 1;

class DisciplinaList extends Component {

    componentDidMount() {

        //chamar o método dos actions
        this.props.listarDisciplina();

        console.log(" LISTA DE DISCIPLINAS = ", this.props.disciplinaLista);
    }

    render() {
        return (
            <div>
                <Link to="/Disciplina/form" className="btn btn-primary"> Novo</Link>
                <table className="table">
                    <thead>
                        <tr>
                            <th scope="col">Nome</th>
                            <th scope="col">Código</th>
                            <th scope="col">Carga Horária</th>
                            <th scope="col">Ações</th>
                        </tr>
                    </thead>
                    <tbody>

                        {
                            this.props.disciplinaLista.map(disciplina =>

                                <tr>
                                    <td>{disciplina.nome}</td>
                                    <td>{disciplina.codigo}</td>
                                    <td>{disciplina.carga_horaria}</td>
                                    <td>{disciplina.carga_horaria}</td>
                                    <td>

                                        <Link to={`/disciplina/form/${id}`} className="btn btn-primary m-10"> Alterar</Link>

                                        <button type="button" className="btn btn-danger" data-toggle="modal" data-target="#exampleModal"> Excluir </button>
                                        <div className="modal fade" id="exampleModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div className="modal-dialog">
                                                <div className="modal-content">
                                                    <div className="modal-header">
                                                        <h5 className="modal-title" id="exampleModalLabel">Confirmação de exclusão!</h5>
                                                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div className="modal-body">
                                                        Deseja realmente excluir a disciplina {disciplina.nome}?
                                            </div>
                                                    <div className="modal-footer">
                                                        <button type="button" className="btn btn-primary" data-dismiss="modal">SIM</button>
                                                        <button type="button" className="btn btn-secondary">NÃO</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            )
                    }
                    </tbody>
                </table>
            </div >
        );
    }
}

export default connect(disciplinaListSelector, { listarDisciplina })(DisciplinaList); 