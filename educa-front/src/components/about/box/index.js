import React from 'react';
import './style.scss';

export const Box = ({
  title,
  description,
}) => (
  <div className="box">
    <div className="box__icon">
      Icon
    </div>
    <div className="box__title">
      { title }
    </div>
    <div className="box__description">
      { description }
    </div>
  </div>
);

export default Box;
