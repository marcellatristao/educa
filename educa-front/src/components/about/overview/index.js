import React from 'react';
import Box from './box/';
import Overview from './overview/';
import './style.scss';

const loremIpsum = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris quis accumsan justo. Maecenas iaculis tincidunt dui, eget aliquam elit malesuada id. Etiam ultricies tincidunt arcu. Nam non nisi nec massa sagittis molestie ac at elit. Donec nec tellus sit amet lacus rhoncus iaculis sed sit amet quam. Curabitur id elementum eros, in gravida urna. Cras vel sapien vel tortor malesuada placerat. Etiam egestas ornare porttitor. Nam nec nunc maximus felis elementum iaculis. Nulla in lorem felis';

export const AboutSection = () => (
  <section className="about">
    <Overview />
    <Box
      title="Melhores práticas"
      description={loremIpsum}
    />
    <Box
      title="Dicas complementares"
      description={loremIpsum}
    />
    <Box
      title="Utilização da tecnologia"
      description={loremIpsum}
    />
    <Box
      title="Utilização da tecnologia"
      description={loremIpsum}
    />
  </section>
);

export default AboutSection;
