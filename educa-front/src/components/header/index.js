import React from 'react';
import './style.scss';

export const Header = () => (
  <div className="header">
    <div className="header__logo">
      EDUCA
    </div>
    <div className="header__menu">
      <div className="header__item">
        Home
      </div>
      <div className="header__item">
        Sobre nós
      </div>
      <div className="header__item header__item--button">
        Login
      </div>
    </div>
  </div>
);

export default Header;
