import React from 'react';
import ReadingWomanFig from '../../assets/reading-woman.png';
import './style.scss';

export const IntroductionSection = () => (
  <section className="introduction">
    <div className="introduction__content">
      <h1 className="introduction__title">
        Expandir o conhecimento e a sua sala de aula em primeiro lugar.
      </h1>
      <p className="introduction__description">
        O EDUCA É um sistema web interativo de aprendizagem onde o educador poderá criar uma sala de aula em ambiente virtual onde será possível a inserção de materiais de apoio, cadastro de questionários e a realização de quizzes. Possibilita que os usuários entrem na “sala de aula” para praticar os exercícios propostos através de um código entregue ao usuário na criação das salas. O diferencial é o gerenciamento e controle das turmas por meio de atividades não monótonas que prendem a atenção do usuário.
      </p>
      <button className="introduction__button">
        Contate-nos
      </button>
    </div>
    <div className="introduction__fig">
      <img src={ReadingWomanFig} />
    </div>
  </section>
);

export default IntroductionSection;
