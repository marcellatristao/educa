import React from 'react';
import Classnames from 'classnames';
import './style.scss';

export const Plan = ({
  edition,
  price,
  description,
  isRecommended = false,
}) => {
  const planClassesName = Classnames(
    'plan',
    {
      'plan--recommended': isRecommended,
    }
  );

  return (
    <div className={planClassesName}>
      { isRecommended && (
        <div className="plan__recommended_tag">
          Recomendado
        </div>
      )}
      <div className="plan__name">
        { edition }
      </div>
      <div className="plan__icon"/>
      <div className="plan__price_container">
        <span>
          R$
        </span>
        <span className="plan__price">
          { price }
        </span>
      </div>
      <div className="plan__description">
        { description }
      </div>
      <button className="plan__purchase_btn">
        Comprar
      </button>
    </div>
  );
}

export default Plan;
