import React from 'react';
import Plan from './plan/';
import './style.scss';

export const PricingSection = () => (
  <section className="pricing">
    <h3 className="pricing__title">
      Preços e opções
    </h3>
    <div className="pricing__plans">
      <Plan
        edition="Edição Regular"
        price="19,90"
        description="Plano basico blablabla"
      />
      <Plan
        edition="Edição P"
        price="29,90"
        isRecommended
        description="Plano basico blablabla"
      />
      <Plan
        edition="Edição Profissional"
        price="119,90"
        description="Plano basico blablabla"
      />
    </div>
  </section>
);

export default PricingSection;
