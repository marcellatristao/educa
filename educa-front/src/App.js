import React from 'react';
import { Provider } from 'react-redux';
import store from "./store/store";
import './App.css';

// HOME
// import Header from './components/header/';
// import AboutSection from './components/about';
// import IntroductionSection from './components/introduction/';
// import PricingSection from './components/pricing/';


import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

import DisciplinaForm from "./views/DisciplinaForm";
import DisciplinaList from "./views/DisciplinaList";

function App() {
  return (
    <div className="App">


{/*   HOME
      <Header />
      <IntroductionSection />
      <AboutSection />
      <PricingSection /> */}


      <Provider store={store}>
        {/* Conteúdo fixo  -> Após clicar em login*/}
        <Router>
          <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <a className="navbar-brand" href="#">EDUCA</a>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
              <div className="navbar-nav">
                <a className="nav-item nav-link active" href="#">Home <span className="sr-only">(current)</span></a>
                <a className="nav-item nav-link" href="#">Features</a>
                <a className="nav-item nav-link" href="#">Pricing</a>
                <a className="nav-item nav-link disabled" href="#" tabIndex="-1" aria-disabled="true">Disabled</a>
              </div>
            </div>
          </nav>

          <div className="row">

            {/* Menu Vertical */}
            <div className="col-2">
              <ul className="nav flex-column">
                <li className="nav-item">
                  <Link to="/Disciplina/List">Listagem</Link>
                </li>
                <li>
                  <Link to="#">Questionários</Link>
                </li>
              </ul>
            </div>


            {/* CONTEÚDO DINÂMICO */}
            <div className="col-10">
              <Switch>
                <Route exact path="/Disciplina/list" component={DisciplinaList} />
                <Route exact path="/Disciplina/form/:id" component={DisciplinaForm} />
                <Route exact path="/Disciplina/form" component={DisciplinaForm} />
              </Switch>
            </div>



          </div>
        </Router>
      </Provider>

    </div>
  );
}

export default App;