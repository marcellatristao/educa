import {DISCIPLINA_ACTIONS} from "../actions/DisciplinaActions";

const disciplinaState = {
    disciplina_lista: [],
    disciplina: {}
}

export default function DisciplinaReducer(state = disciplinaState, action){

    switch(action.type){
        
        case DISCIPLINA_ACTIONS.LISTAR:
        return {
            ...state,
            disciplina_lista: action.payload
        }

        case DISCIPLINA_ACTIONS.BUSCAR:
        return{
            ...state,
            disciplina: action.payload
        }

        case DISCIPLINA_ACTIONS.SALVAR:
        return state;

        case DISCIPLINA_ACTIONS.ALTERAR:
        return state;

        case DISCIPLINA_ACTIONS.EXCLUIR:
        return state;

        default:
            return state;
    }

}