export const DISCIPLINA_ACTIONS = {

    LISTAR: "DISCIPLINA_LISTAR",
    BUSCAR: "DISCIPLINA_BUSCAR",
    ALTERAR: "DISCIPLINA_ALTERAR",
    EXCLUIR: "DISCIPLINA_EXCLUIR"

}

export function salvarDisciplina(disciplina){

    console.log("ACTIONS = ", disciplina)

}

export function listarDisciplina(){

    //callback
    return function(dispatch){
        
        return dispatch({
            type: DISCIPLINA_ACTIONS.LISTAR,
            payload: [
                {
                    "nome": "Matemática Discreta",
                    "codigo": "2020122",
                    "carga_horaria": "4horas semanais"
                },
                {
                    "nome": "Matemática Básica",
                    "codigo": "2020123",
                    "carga_horaria": "4horas semanais"
                },
            ]
        });
    }
}

export function buscarDisciplinaNome(nome){

    return function (dispatch){
        return dispatch({
            type: DISCIPLINA_ACTIONS.BUSCAR,
            payload: {}
        });
    }
}