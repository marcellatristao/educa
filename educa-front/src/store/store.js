import {combineReducers, createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';

import DisciplinaReducer from "./reducers/DisciplinaReducer";

//combinar todos os reducers do projeto
const reducers = combineReducers({
    disciplinaState: DisciplinaReducer
});

const localState = localStorage.getItem('applicationState')
? JSON.parse(localStorage.getItem('applicationState'))
: [];

const store = createStore(
    reducers,
    localStorage,
    applyMiddleware(thunk)
);

store.subscribe ( function () {
    localStorage.setItem('applicationState', JSON.stringify(store.getState()));

});

export default store;