import values from "lodash/values";

export const disciplinaListSelector = ({disciplinaState}) => ({ disciplinaLista: values(disciplinaState. disciplina_lista) });


export const disciplinaSelector = ({disciplinaState}) => ({ disciplina: disciplinaState.disciplina});